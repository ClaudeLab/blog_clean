package fr.lusseau.infrastructure.dao;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.infrastructure.dao.qualifier.UserDaoQualifier;
import fr.lusseau.infrastructure.entity.UserEntity;

import javax.inject.Inject;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.infrastructure.dao
 * @date 14/08/2022
 */
class UserDaoImplTest {


    private final IAbstractCrudDaoFactory<UserEntity> crudDaoFactory;

    @Inject
    public UserDaoImplTest(@UserDaoQualifier IAbstractCrudDaoFactory<UserEntity> crudDaoFactory) {
        this.crudDaoFactory = crudDaoFactory;
    }

//    @Test
//    void createUser() {
//        UserEntity user = UserEntity.builder()
//                .withEmail("john.doe@lost.com")
//                .withFirstName("John")
//                .withLastName("Doe")
//                .withPassword("what")
//                .withRole(Role.SUBSCRIBER).build();
//        crudDaoFactory.create(user);
//        Assertions.assertNotNull(user);
//    }

//    @Test
//    void editUser() {
//        UserEntity user = UserEntity.builder()
//                .withId(12L)
//                .withEmail("john.doe@lost.com")
//                .withFirstName("John")
//                .withLastName("Doe")
//                .withPassword("what")
//                .withRole(Role.SUBSCRIBER).build();
//        crudDaoFactory.update(user);
//        Assertions.assertThat(user).isNotNull();
//    }
}
