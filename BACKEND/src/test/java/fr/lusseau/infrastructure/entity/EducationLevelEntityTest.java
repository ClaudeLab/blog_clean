package fr.lusseau.infrastructure.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.entity
 * @date 11/08/2022
 */
class EducationLevelEntityTest {

    @Test
    void shouldGetEducationLevelInformation() {
        EducationLevelEntity article = EducationLevelEntity.builder()
                .withId(1L)
                .withName("Master degree")
                .build();
        Assertions.assertNotNull(article);
    }
}