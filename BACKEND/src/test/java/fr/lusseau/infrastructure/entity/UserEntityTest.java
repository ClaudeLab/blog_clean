package fr.lusseau.infrastructure.entity;

import fr.lusseau.domain.model.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.entity
 * @date 11/08/2022
 */
class UserEntityTest {

    @Test
    void shouldGetUserInformation() {
        UserEntity article = UserEntity.builder()
                .withId(2L)
                .withEmail("test@test.test")
                .withFirstName("John")
                .withLastName("Doe")
                .withPassword("lost")
                .withRole(Role.ADMINISTRATOR)
                .build();
        Assertions.assertNotNull(article);
    }
}