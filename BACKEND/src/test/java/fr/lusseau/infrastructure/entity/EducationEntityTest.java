package fr.lusseau.infrastructure.entity;

import fr.lusseau.domain.model.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.entity
 * @date 11/08/2022
 */
class EducationEntityTest {

    @Test
    void shouldGetEducationInformation() {
        EducationEntity article = EducationEntity.builder()
                .withTitle("test title")
                .withBody("Body test")
                .withUrl("test url")
                .withDateIn(LocalDateTime.of(2022,12,25,12,00,00))
                .withCreatedAt(LocalDateTime.now())
                .withAuthor(UserEntity.builder()
                        .withId(2L)
                        .withEmail("test@test.test")
                        .withFirstName("John")
                        .withLastName("Doe")
                        .withPassword("lost")
                        .withRole(Role.SUBSCRIBER)
                        .build())
                .build();
        Assertions.assertNotNull(article);
    }
}