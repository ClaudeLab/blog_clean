package fr.lusseau.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.model
 * @date 11/08/2022
 */
class EducationTest {

    @Test
    void shouldGetEducationArticleInformation() {
        Education education = Education.builder().build();
        Assertions.assertNotNull(education);
    }
}
