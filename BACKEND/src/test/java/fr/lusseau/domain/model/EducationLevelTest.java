package fr.lusseau.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.model
 * @date 11/08/2022
 */
class EducationLevelTest {

    @Test
    void shouldGetEducationLevelInformation() {
        EducationLevel level =  EducationLevel.builder().build();
        Assertions.assertNotNull(level);
    }
}
