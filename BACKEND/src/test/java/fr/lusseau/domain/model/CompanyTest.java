package fr.lusseau.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.model
 * @date 11/08/2022
 */
class CompanyTest {

    @Test
    void shouldGetCompanyInformation() {
        Company company = Company.builder()
                .withId(1L)
                .withName("Kereval")
                .withType("Laboratoire de test logiciel")
                .withPlace("Rennes")
                .build();
        Assertions.assertNotNull(company);
    }
}
