package fr.lusseau.infrastructure.dao.impl;

import fr.lusseau.application.dao.IEducationDao;
import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.infrastructure.dao.qualifier.EducationDaoQualifier;
import fr.lusseau.infrastructure.entity.EducationEntity;
import fr.lusseau.infrastructure.factory.FactoryService;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.infrastructure.dao
 * @date 15/08/2022
 */
@Named("EducationDaoImpl")
@LogAudited
@ApplicationScoped
@EducationDaoQualifier
public class EducationDaoImpl implements IAbstractCrudDaoFactory<EducationEntity>, IEducationDao {

    private final FactoryService factoryService;

    @Inject
    public EducationDaoImpl(FactoryService factoryService) {
        this.factoryService = factoryService;
    }

    @Override
    public List<EducationEntity> getAll() {
        return factoryService.createEntityManager().createQuery("FROM EducationEntity").getResultList();
    }

    @Override
    public EducationEntity getOne(Long id) {
        return factoryService.createEntityManager().find(EducationEntity.class, id);
    }

    @Override
    public void create(EducationEntity educationEntity) {
        factoryService.createEntityManager().persist(educationEntity);
    }

    @Override
    public void update(EducationEntity educationEntity) {
        factoryService.createEntityManager().merge(educationEntity);
    }

    @Override
    public void remove(EducationEntity educationEntity) {
        educationEntity = getOne(educationEntity.getId());
        factoryService.createEntityManager().remove(educationEntity);
    }

    @Override
    public List<EducationEntity> isTitleExist(String title) {
        return factoryService.createEntityManager().createNamedQuery("Education.isTitleExist").setParameter("title", title).getResultList();
    }

    @Override
    public List<EducationEntity> isUrlExist(String url) {
        return factoryService.createEntityManager().createNamedQuery("Education.isUrlExist").setParameter("url", url).getResultList();
    }

}