package fr.lusseau.infrastructure.dao.impl;

import fr.lusseau.application.dao.ICompanyDao;
import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.infrastructure.dao.qualifier.CompanyDaoQualifier;
import fr.lusseau.infrastructure.entity.CompanyEntity;
import fr.lusseau.infrastructure.factory.FactoryService;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.infrastructure.dao
 * @date 27/08/2022
 */
@Named("CompanyDaoImpl")
@LogAudited
@ApplicationScoped
@CompanyDaoQualifier
public class CompanyDaoImpl implements IAbstractCrudDaoFactory<CompanyEntity>, ICompanyDao {

    private final FactoryService factoryService;

    @Inject
    public CompanyDaoImpl(FactoryService factoryService) {
        this.factoryService = factoryService;
    }

    @Override
    public List<CompanyEntity> getAll() {
        return factoryService.createEntityManager().createQuery("FROM CompanyEntity").getResultList();
    }

    @Override
    public CompanyEntity getOne(Long id) {
        return factoryService.createEntityManager().find(CompanyEntity.class, id);
    }

    @Override
    public void create(CompanyEntity companyEntity) {
        factoryService.createEntityManager().persist(companyEntity);
    }

    @Override
    public void update(CompanyEntity companyEntity) {
        factoryService.createEntityManager().merge(companyEntity);
    }

    @Override
    public void remove(CompanyEntity companyEntity) {
        companyEntity = getOne(companyEntity.getId());
        factoryService.createEntityManager().remove(companyEntity);
    }

    @Override
    public List<CompanyEntity> isNameExist(String name) {
        return factoryService.createEntityManager().createNamedQuery("Company.isNameExist").setParameter("name", name).getResultList();
    }

}