package fr.lusseau.infrastructure.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.model
 * @date 10/08/2022
 */
@Entity(name = "Company")
@Table(name = "company")
@NamedQuery(name = "Company.isNameExist", query = "SELECT c FROM Company c WHERE name = :name")
public class CompanyEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "compGen")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "place")
    private String place;
    @Column(name = "type")
    private String type;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "company")
    @JsonbTransient
    private List<EducationEntity> educationEntityList;

    protected CompanyEntity() {
    }

    public CompanyEntity(CompanyEntityBuilder builder) {
        id = builder.id;
        name = builder.name;
        place = builder.place;
        type = builder.type;
        educationEntityList = builder.educationEntityList;
    }

    public static CompanyEntityBuilder builder() {
        return new CompanyEntityBuilder();
    }

    public static class CompanyEntityBuilder {
        private Long id;
        private String name;
        private String place;
        private String type;
        private List<EducationEntity> educationEntityList;

        public CompanyEntityBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public CompanyEntityBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public CompanyEntityBuilder withPlace(String place) {
            this.place = place;
            return this;
        }

        public CompanyEntityBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public CompanyEntityBuilder withEducationList(List<EducationEntity> educationEntityList) {
            this.educationEntityList = educationEntityList;
            return this;
        }

        public CompanyEntity build() {
            return new CompanyEntity(this);
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public String getType() {
        return type;
    }

    public List<EducationEntity> getEducationEntityList() {
        return educationEntityList;
    }

    public void setEducationEntityList(List<EducationEntity> educationEntityList) {
        this.educationEntityList = educationEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyEntity that = (CompanyEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(place, that.place) && Objects.equals(type, that.type) && Objects.equals(educationEntityList, that.educationEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, place, type, educationEntityList);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("fr.lusseau.infrastructure.dto.CompanyDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", place='").append(place).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }
}