package fr.lusseau.infrastructure.entity;

import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.model
 * @date 10/08/2022
 */
@Entity(name = "Level")
@LogAudited
@Table(name = "level", schema = "level")
@Cacheable
@NamedQuery(name = "EducationLevel.findByName", query = "SELECT e FROM Level e WHERE e.name = :name")
public class EducationLevelEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "levelGen")
    private Long id;
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "level")
    @JsonbTransient
    private List<EducationEntity> educationEntityList;


    protected EducationLevelEntity() {
    }

    public EducationLevelEntity(EducationLevelEntityBuilder builder) {
        id = builder.id;
        name = builder.name;
        educationEntityList = builder.educationEntityList;
    }


    public static EducationLevelEntityBuilder builder() {
        return new EducationLevelEntityBuilder();
    }

    public static class EducationLevelEntityBuilder {
        private Long id;
        private String name;
        private List<EducationEntity> educationEntityList;

        public EducationLevelEntityBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public EducationLevelEntityBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public EducationLevelEntityBuilder withEducationList(List<EducationEntity> educationEntityList) {
            this.educationEntityList = educationEntityList;
            return this;
        }

        public EducationLevelEntity build() {
            return new EducationLevelEntity(this);
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<EducationEntity> getEducationEntityList() {
        return educationEntityList;
    }

    public void setEducationEntityList(List<EducationEntity> educationEntityList) {
        this.educationEntityList = educationEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EducationLevelEntity that = (EducationLevelEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(educationEntityList, that.educationEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, educationEntityList);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EducationLevelEntity{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}