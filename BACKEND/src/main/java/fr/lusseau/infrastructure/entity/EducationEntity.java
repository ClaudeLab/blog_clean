package fr.lusseau.infrastructure.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.entity
 * @date 06/08/2022
 */
@Entity(name = "Education")
@Table(name = "education")
@NamedQuery(name = "Education.isTitleExist", query = "SELECT e FROM Education e WHERE title = :title")
@NamedQuery(name = "Education.isUrlExist", query = "SELECT e FROM Education e WHERE url = :url")
public class EducationEntity extends ArticleEntity implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_company", nullable = false, referencedColumnName = "id")
    @JsonbTransient
    private CompanyEntity company;
    @Column(name = "dateIn")
    private LocalDateTime dateIn;
    @Column(name = "dateOut")
    private LocalDateTime dateOut;
    @ManyToOne
    @JoinColumn(name = "id_article")
    @JsonbTransient
    private EducationLevelEntity level;

    public EducationEntity() {
    }

    public EducationEntity(EducationEntityBuilder builder) {
        super(builder);
        company = builder.company;
        dateIn = builder.dateIn;
        dateOut = builder.dateOut;
        level = builder.level;
    }

    public static EducationEntityBuilder builder() {
        return new EducationEntityBuilder();
    }

    public static class EducationEntityBuilder extends ArticleEntityBuilder<EducationEntityBuilder> {
        private CompanyEntity company;
        private LocalDateTime dateIn;
        private LocalDateTime dateOut;
        private EducationLevelEntity level;

        @Override
        public EducationEntityBuilder getThis() {
            return this;
        }

        public EducationEntityBuilder withCompany(CompanyEntity company) {
            this.company = company;
            return this;
        }

        public EducationEntityBuilder withDateIn(LocalDateTime dateIn) {
            this.dateIn = dateIn;
            return this;
        }

        public EducationEntityBuilder withDateOut(LocalDateTime dateOut) {
            this.dateOut = dateOut;
            return this;
        }

        public EducationEntityBuilder withLevel(EducationLevelEntity level) {
            this.level = level;
            return this;
        }

        public EducationEntity build() {
            return new EducationEntity(this);
        }
    }


    public CompanyEntity getCompany() {
        return company;
    }

    public LocalDateTime getDateIn() {
        return dateIn;
    }

    public LocalDateTime getDateOut() {
        return dateOut;
    }

    public EducationLevelEntity getLevel() {
        return level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EducationEntity that = (EducationEntity) o;
        return Objects.equals(company, that.company) && Objects.equals(dateIn, that.dateIn) && Objects.equals(dateOut, that.dateOut) && Objects.equals(level, that.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), company, dateIn, dateOut, level);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EducationEntity{");
        sb.append(super.toString());
        sb.append("company=").append(company);
        sb.append(", dateIn=").append(dateIn);
        sb.append(", dateOut=").append(dateOut);
        sb.append(", level=").append(level);
        sb.append('}');
        return sb.toString();
    }
}