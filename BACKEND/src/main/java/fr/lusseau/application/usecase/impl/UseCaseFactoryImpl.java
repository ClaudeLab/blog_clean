package fr.lusseau.application.usecase.impl;

import fr.lusseau.application.factory.IUseCaseFactory;
import fr.lusseau.application.usecase.ICheckUseCase;
import fr.lusseau.application.usecase.check.CheckUseCase;
import fr.lusseau.infrastructure.factory.FactoryService;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase
 * @date 08/09/2022
 */
@LogAudited
@Named
public class UseCaseFactoryImpl implements IUseCaseFactory {

    private final FactoryService factoryService;

    @Inject
    public UseCaseFactoryImpl(FactoryService factoryService) {
        this.factoryService = factoryService;
    }

    @Override
    public ICheckUseCase getCheckUseCase() {
        return new CheckUseCase(factoryService);
    }
}
