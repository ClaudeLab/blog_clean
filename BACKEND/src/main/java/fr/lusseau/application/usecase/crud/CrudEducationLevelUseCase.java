package fr.lusseau.application.usecase.crud;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.application.factory.IAbstractCrudUseCaseFactory;
import fr.lusseau.application.usecase.qualifier.EducationLevelCrudUseCaseQualifier;
import fr.lusseau.domain.model.EducationLevel;
import fr.lusseau.domain.validator.EducationLevelValidator;
import fr.lusseau.infrastructure.dao.qualifier.EducationLevelDaoQualifier;
import fr.lusseau.infrastructure.entity.EducationLevelEntity;
import fr.lusseau.infrastructure.mapper.IEducationLevelMapper;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase.impl
 * @date 29/08/2022
 */
@Named
@LogAudited
@ApplicationScoped
@EducationLevelCrudUseCaseQualifier
public class CrudEducationLevelUseCase implements IAbstractCrudUseCaseFactory<EducationLevel> {

    @EducationLevelDaoQualifier
    private IAbstractCrudDaoFactory<EducationLevelEntity> dao;


    @Override
    public EducationLevel create(EducationLevel educationLevel) {
        EducationLevelEntity educationLevelEntity = IEducationLevelMapper.INSTANCE.educationLevelToEducationLevelDto(educationLevel);
        dao.create(educationLevelEntity);
        if (educationLevelEntity.getId() == null) {
            return null;
        }
        return IEducationLevelMapper.INSTANCE.educationLevelDtoToEducationLevel(educationLevelEntity);
    }

    @Override
    public EducationLevel getOne(Long id) {
        EducationLevelEntity educationLevelLevelDTO = dao.getOne(id);
        if (educationLevelLevelDTO == null) {
            return null;
        }
        return IEducationLevelMapper.INSTANCE.educationLevelDtoToEducationLevel(educationLevelLevelDTO);
    }

    @Override
    public List<EducationLevel> getAll() {
        List<EducationLevelEntity> educationLevelLevelDTOS = dao.getAll();
        if (educationLevelLevelDTOS.isEmpty()) {
            return Collections.emptyList();
        }
        return IEducationLevelMapper.INSTANCE.educationLevelDtoListToEducationLevelList(educationLevelLevelDTOS);
    }

    @Override
    public void update(EducationLevel educationLevel) {
        EducationLevelValidator.validateEducationLevel(educationLevel);
        EducationLevelEntity educationLevelEntity= IEducationLevelMapper.INSTANCE.educationLevelToEducationLevelDto(educationLevel);
        dao.update(educationLevelEntity);
    }

    @Override
    public void remove(EducationLevel educationLevel) {
        dao.remove(IEducationLevelMapper.INSTANCE.educationLevelToEducationLevelDto(educationLevel));
    }
}
