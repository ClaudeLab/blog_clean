package fr.lusseau.application.usecase.crud;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.application.factory.IAbstractCrudUseCaseFactory;
import fr.lusseau.application.usecase.qualifier.CompanyCrudUseCaseQualifier;
import fr.lusseau.domain.model.Company;
import fr.lusseau.domain.validator.CompanyValidator;
import fr.lusseau.infrastructure.dao.qualifier.CompanyDaoQualifier;
import fr.lusseau.infrastructure.entity.CompanyEntity;
import fr.lusseau.infrastructure.mapper.ICompanyMapper;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase.impl
 * @date 29/08/2022
 */
@Named
@LogAudited
@ApplicationScoped
@CompanyCrudUseCaseQualifier
public class CrudCompanyUseCase implements IAbstractCrudUseCaseFactory<Company> {

    @CompanyDaoQualifier
    private IAbstractCrudDaoFactory<CompanyEntity> dao;


    @Override
    public Company create(Company company) {
        CompanyEntity companyEntity = ICompanyMapper.INSTANCE.companyToCompanyDto(company);
        dao.create(companyEntity);
        if (companyEntity.getId() == null) {
            return null;
        }
        return ICompanyMapper.INSTANCE.companyDtoToCompany(companyEntity);
    }

    @Override
    public Company getOne(Long id) {
        CompanyEntity companyEntity = dao.getOne(id);
        if (companyEntity == null) {
            return null;
        }
        return ICompanyMapper.INSTANCE.companyDtoToCompany(companyEntity);
    }


    @Override
    public List<Company> getAll() {
        List<CompanyEntity> companyEntities = dao.getAll();
        if (companyEntities.isEmpty()) {
            return Collections.emptyList();
        }
        return ICompanyMapper.INSTANCE.companyDtoListToCompanyList(companyEntities);
    }

    @Override
    public void update(Company company) {
        CompanyValidator.validateCompany(company);
        CompanyEntity companyEntity = ICompanyMapper.INSTANCE.companyToCompanyDto(company);
        dao.update(companyEntity);
    }

    @Override
    public void remove(Company company) {
        dao.remove(ICompanyMapper.INSTANCE.companyToCompanyDto(company));
    }


}
