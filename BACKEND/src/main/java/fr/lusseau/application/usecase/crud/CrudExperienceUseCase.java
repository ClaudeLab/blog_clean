package fr.lusseau.application.usecase.crud;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.application.factory.IAbstractCrudUseCaseFactory;
import fr.lusseau.application.usecase.qualifier.ExperienceCrudUseCaseQualifier;
import fr.lusseau.domain.model.Experience;
import fr.lusseau.domain.validator.ExperienceValidator;
import fr.lusseau.infrastructure.dao.qualifier.ExperienceDaoQualifier;
import fr.lusseau.infrastructure.entity.ExperienceEntity;
import fr.lusseau.infrastructure.mapper.IExperienceMapper;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase.impl
 * @date 29/08/2022
 */
@Named
@LogAudited
@ApplicationScoped
@ExperienceCrudUseCaseQualifier
public class CrudExperienceUseCase implements IAbstractCrudUseCaseFactory<Experience> {

    @ExperienceDaoQualifier
    private IAbstractCrudDaoFactory<ExperienceEntity> dao;


    @Override
    public Experience create(Experience education) {
        ExperienceEntity experienceEntity = IExperienceMapper.INSTANCE.experienceToExperienceDto(education);
        dao.create(experienceEntity);
        return education;
    }

    @Override
    public Experience getOne(Long id) {
        ExperienceEntity experienceEntity =dao.getOne(id);
        if (experienceEntity == null) {
            return null;
        }
        return IExperienceMapper.INSTANCE.experienceDtoToExperience(experienceEntity);
    }

    @Override
    public List<Experience> getAll() {
        List<ExperienceEntity> experienceEntities = dao.getAll();
        if (experienceEntities.isEmpty()) {
            return Collections.emptyList();
        }
        return IExperienceMapper.INSTANCE.experienceDtoListToExperienceList(experienceEntities);
    }

    @Override
    public void update(Experience education) {
        ExperienceValidator.validateExperienceArticle(education);
        ExperienceEntity experienceEntity = IExperienceMapper.INSTANCE.experienceToExperienceDto(education);
        dao.update(experienceEntity);
    }

    @Override
    public void remove(Experience education) {
        dao.remove(IExperienceMapper.INSTANCE.experienceToExperienceDto(education));
    }
}
