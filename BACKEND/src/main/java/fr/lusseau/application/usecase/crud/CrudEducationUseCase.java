package fr.lusseau.application.usecase.crud;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.application.factory.IAbstractCrudUseCaseFactory;
import fr.lusseau.application.usecase.qualifier.EducationCrudUseCaseQualifier;
import fr.lusseau.domain.model.Education;
import fr.lusseau.domain.validator.EducationValidator;
import fr.lusseau.infrastructure.dao.qualifier.EducationDaoQualifier;
import fr.lusseau.infrastructure.entity.EducationEntity;
import fr.lusseau.infrastructure.mapper.IEducationMapper;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase.impl
 * @date 29/08/2022
 */
@Named
@LogAudited
@ApplicationScoped
@EducationCrudUseCaseQualifier
public class CrudEducationUseCase implements IAbstractCrudUseCaseFactory<Education> {

    @EducationDaoQualifier
    private IAbstractCrudDaoFactory<EducationEntity> dao;


    @Override
    public Education create(Education education) {
        EducationEntity educationEntity = IEducationMapper.INSTANCE.educationToEducationDto(education);
        dao.create(educationEntity);
        return education;
    }

    @Override
    public Education getOne(Long id) {
        EducationEntity educationEntity = dao.getOne(id);
        if (educationEntity == null) {
            return null;
        }
        return IEducationMapper.INSTANCE.educationDtoToEducation(educationEntity);
    }

    @Override
    public List<Education> getAll() {
        List<EducationEntity> educationDTOS = dao.getAll();
        if (educationDTOS.isEmpty()) {
            return Collections.emptyList();
        }
        return IEducationMapper.INSTANCE.educationDtoListToEducationList(educationDTOS);
    }

    @Override
    public void update(Education education) {
        EducationValidator.validateEducationArticle(education);
        EducationEntity educationDTO = IEducationMapper.INSTANCE.educationToEducationDto(education);
        dao.update(educationDTO);
    }

    @Override
    public void remove(Education education) {
        dao.remove(IEducationMapper.INSTANCE.educationToEducationDto(education));
    }
}
