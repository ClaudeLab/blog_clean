package fr.lusseau.application.usecase.crud;

import fr.lusseau.application.factory.IAbstractCrudDaoFactory;
import fr.lusseau.application.factory.IAbstractCrudUseCaseFactory;
import fr.lusseau.application.usecase.qualifier.UserCrudUseCaseQualifier;
import fr.lusseau.domain.model.User;
import fr.lusseau.domain.validator.UserValidator;
import fr.lusseau.infrastructure.dao.qualifier.UserDaoQualifier;
import fr.lusseau.infrastructure.entity.UserEntity;
import fr.lusseau.infrastructure.mapper.IUserMapper;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.usecase.impl
 * @date 29/08/2022
 */
@Named
@LogAudited
@ApplicationScoped
@UserCrudUseCaseQualifier
public class CrudUserUseCase implements IAbstractCrudUseCaseFactory<User> {

    @UserDaoQualifier
    private IAbstractCrudDaoFactory<UserEntity> dao;


    @Override
    public User create(User user) {
        UserEntity userEntity = IUserMapper.INSTANCE.userToUserDto(user);
        dao.create(userEntity);
        return user;
    }

    @Override
    public User getOne(Long id) {
        UserEntity userEntity =dao.getOne(id);
        if (userEntity == null) {
            return null;
        }
        return IUserMapper.INSTANCE.userDtoToUser(userEntity);
    }

    @Override
    public List<User> getAll() {
        List<UserEntity> experienceEntities = dao.getAll();
        if (experienceEntities.isEmpty()) {
            return Collections.emptyList();
        }
        return IUserMapper.INSTANCE.userDtoListToUserList(experienceEntities);
    }

    @Override
    public void update(User user) {
        UserValidator.validateUser(user);
        UserEntity userEntity = IUserMapper.INSTANCE.userToUserDto(user);
        dao.update(userEntity);
    }

    @Override
    public void remove(User user) {
        dao.remove(IUserMapper.INSTANCE.userToUserDto(user));
    }
}
