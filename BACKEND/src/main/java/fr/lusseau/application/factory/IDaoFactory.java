package fr.lusseau.application.factory;

import fr.lusseau.application.dao.*;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.factory
 * @date 08/09/2022
 */
public interface IDaoFactory {

    IUserDao getUserDao();

    ICompanyDao getCompanyDao();

    IEducationDao getEducationDao();

    IExperienceDao getExperienceDao();

    IEducationLevelDao getEducationLevelDao();
}
