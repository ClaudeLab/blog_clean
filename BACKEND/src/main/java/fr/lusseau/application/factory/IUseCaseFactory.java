package fr.lusseau.application.factory;

import fr.lusseau.application.usecase.ICheckUseCase;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.application.factory
 * @date 08/09/2022
 */
public interface IUseCaseFactory {

    ICheckUseCase getCheckUseCase();
}
