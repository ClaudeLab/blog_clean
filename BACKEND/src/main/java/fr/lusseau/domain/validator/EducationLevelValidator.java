package fr.lusseau.domain.validator;

import fr.lusseau.domain.exception.ValidatorException;
import fr.lusseau.domain.model.EducationLevel;

import java.io.Serializable;

import static java.util.Objects.isNull;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.validator
 * @date 16/08/2022
 */
public class EducationLevelValidator implements Serializable {

    static String nameRequired = "nameRequired";

    public static void validateEducationLevel(final EducationLevel educationLevel) {

        if (isNull(educationLevel.getName())) throw new ValidatorException(nameRequired);
        if ((educationLevel.getName().isBlank())) throw new ValidatorException(nameRequired);

    }
}
