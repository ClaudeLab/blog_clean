package fr.lusseau.domain.validator;

import fr.lusseau.domain.exception.ValidatorException;
import fr.lusseau.domain.model.Company;

import java.io.Serializable;

import static java.util.Objects.isNull;


/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.validator
 * @date 16/08/2022
 */
public class CompanyValidator implements Serializable {

    static String companyNameRequired = "companyNameRequired";

    static String companyPlaceRequired = "companyPlaceRequired";


    public static void validateCompany(final Company company) {

        if ((company.getName().isBlank())) throw new ValidatorException(companyNameRequired);
        if (isNull(company.getName())) throw new ValidatorException(companyNameRequired);
        if ((company.getPlace().isBlank())) throw new ValidatorException(companyPlaceRequired);
        if (isNull(company.getPlace())) throw new ValidatorException(companyPlaceRequired);
    }
}
