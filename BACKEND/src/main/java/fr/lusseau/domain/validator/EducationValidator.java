package fr.lusseau.domain.validator;

import fr.lusseau.domain.exception.ValidatorException;
import fr.lusseau.domain.model.Education;
import fr.lusseau.infrastructure.utils.annotation.LogAudited;

import java.io.Serializable;

import static java.util.Objects.isNull;

/**
 * @author Claude Lusseau
 * @project site_perso
 * @package fr.lusseau.domain.validator
 * @date 16/08/2022
 */
@LogAudited
public class EducationValidator extends ArticleValidator implements Serializable {

    static String companyRequired = "companyRequired";

    static String dateInRequired = "dateInRequired";

    public static void validateEducationArticle(final Education education) {

        if (isNull(education.getAuthor())) throw new ValidatorException(authorRequired);

        if ((education.getUrl().isBlank())) throw new ValidatorException(urlRequired);
        if (isNull(education.getUrl())) throw new ValidatorException(urlRequired);

        if ((education.getTitle().isBlank())) throw new ValidatorException(titleRequired);
        if (isNull(education.getTitle())) throw new ValidatorException(titleRequired);

        if (isNull(education.getCreatedAt())) throw new ValidatorException(createdAtRequired);
        if (isNull(education.getDateIn())) throw new ValidatorException(dateInRequired);
        if (isNull(education.getCompany())) throw new ValidatorException(companyRequired);
    }
}
